package com.sales.web.Api.payload;
import lombok.Data;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class AddressDto {
    @NotEmpty(message = "Please provide a street name")
    private String street_name;
    private String apartment;
    @NotEmpty(message = "Please provide a city")
    private String city;
    @NotNull(message = "Please provide a zip code")
    private int zip_code;
    private String suburb;
}
