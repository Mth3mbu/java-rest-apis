package com.sales.web.Api.payload;

import lombok.Data;

@Data
public class OrderStatusRequest {
    private long order_id;
    private long status_id;
}
