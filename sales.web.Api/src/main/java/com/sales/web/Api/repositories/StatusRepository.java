package com.sales.web.Api.repositories;

import com.sales.web.Api.models.Status;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusRepository extends JpaRepository<Status,Long> {
}
