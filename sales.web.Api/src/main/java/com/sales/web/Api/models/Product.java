package com.sales.web.Api.models;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(nullable = false, length = 150)
    private String name;
    @Column(length = 500)
    private String description;
    @Column(nullable = false, length = Integer.MAX_VALUE)
    private String image;
    @Column(nullable = false)
    private double price;
    @Column(nullable = false)
    private int quantity;
}
