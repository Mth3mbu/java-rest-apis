package com.sales.web.Api.controllers;

import com.sales.web.Api.responses.OrderResponse;
import com.sales.web.Api.services.interfaces.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "Orders Controller exposes orders CRUD operation ")
@RestController
@RequestMapping("/api/order")
public class OrdersController {
    @Autowired
    private OrderService orderService;

    @ApiOperation(value = "Returns all orders from the database")
    @GetMapping
    private ResponseEntity<List<OrderResponse>> getAllOrders(){
        return new ResponseEntity<>(orderService.getAllOrders(), HttpStatus.OK);
    }

    @ApiOperation(value = "Returns all orders for a specific user")
    @GetMapping("/{userId}")
    private ResponseEntity<List<OrderResponse>> getOrdersByUserId(@PathVariable(name = "userId") long userId){
        return new ResponseEntity<>(orderService.getByUserId(userId), HttpStatus.OK);
    }
}
