package com.sales.web.Api.payload;

import lombok.Data;
import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class ProductDto {
    @NotEmpty(message = "Please provide a product name")
    private String name;
    @Size(min = 2, message = "product name should at least have a minimum of 2 characters")
    @NotEmpty(message = "Please provide a product description")
    @Size(max = 500, min = 10, message = "product description should at least have 10 to 500 characters")
    private String description;
    @NotEmpty(message = "Please provide a product image")
    @Size(min = 64, message = "product image should at least have 64 characters")
    private String image;
    @Column(nullable = false)
    @NotNull(message = "Please provide a product price")
    private double price;
    @NotNull(message = "Please provide a product quantity")
    @Column(nullable = false)
    private int quantity;
}
