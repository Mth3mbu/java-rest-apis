package com.sales.web.Api.services;

import com.sales.web.Api.exceptions.ResourceNotFoundException;
import com.sales.web.Api.models.Address;
import com.sales.web.Api.payload.AddressDto;
import com.sales.web.Api.repositories.AddressRepository;
import com.sales.web.Api.repositories.UserRepository;
import com.sales.web.Api.services.interfaces.AddressService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SalesAppAddressService implements AddressService {
    @Autowired
    private ModelMapper mapper;
    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    public AddressDto createAddress(AddressDto addressDto, long userId) {
        var user = userRepository.findById(userId)
                .orElseThrow(()-> new ResourceNotFoundException("User","With id",String.format("%s",userId)));

        var newAddress = mapper.map(addressDto, Address.class);

        newAddress.setUser(user);

        var address = addressRepository.save(newAddress);

        return  mapper.map(address, AddressDto.class);
    }
}
