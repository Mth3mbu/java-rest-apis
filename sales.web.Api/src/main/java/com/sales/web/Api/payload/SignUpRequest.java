package com.sales.web.Api.payload;
import lombok.Data;
import javax.validation.Valid;

@Data
public class SignUpRequest {

    @Valid
    private UserDto user;
    @Valid
    private AddressDto address;
}
