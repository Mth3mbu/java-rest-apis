package com.sales.web.Api.services;

import com.sales.web.Api.exceptions.ResourceNotFoundException;
import com.sales.web.Api.models.OrderItem;
import com.sales.web.Api.payload.OrderItemDto;
import com.sales.web.Api.payload.OrderItemRequest;
import com.sales.web.Api.repositories.OrderItemRepository;
import com.sales.web.Api.repositories.OrderRepository;
import com.sales.web.Api.repositories.ProductRepository;
import com.sales.web.Api.services.interfaces.OrderItemService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SalesAppOrderItemService implements OrderItemService {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderItemRepository orderItemRepository;
    @Autowired
    private ModelMapper mapper;

    @Override
    public List<OrderItemDto> createOrderItems(List<OrderItemRequest> orderItemsRequest) {

        var items = mapToOrdersItems(orderItemsRequest);
        var res = orderItemRepository.saveAll(items);

        return mapper.map(res, new ArrayList<OrderItemDto>().getClass());
    }

    private List<OrderItem> mapToOrdersItems(List<OrderItemRequest> orderItemsRequest) {

        var productIds = orderItemsRequest.stream()
                .map(request-> request.getProduct_id())
                .collect(Collectors.toList());

        var orderId = orderItemsRequest.get(0).getOrder_id();

        var order = orderRepository.findById(orderId)
                .orElseThrow(()->
                        new ResourceNotFoundException("Order","id",String.format("%s",orderId)));

        var products = productRepository.findAllById(productIds);

        return orderItemsRequest.stream().map(item-> {

            var prod =  products.stream()
                    .filter(product -> product.getId()==item.getProduct_id())
                    .findFirst().get();

            var orderItem = new OrderItem();

            orderItem.setOrder(order);
            orderItem.setProduct(prod);
            orderItem.setQuantity(item.getQuantity());

            return  orderItem;

        }).collect(Collectors.toList());
    }
}
