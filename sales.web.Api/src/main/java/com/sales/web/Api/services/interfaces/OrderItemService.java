package com.sales.web.Api.services.interfaces;

import com.sales.web.Api.payload.OrderItemDto;
import com.sales.web.Api.payload.OrderItemRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface OrderItemService {
    List<OrderItemDto> createOrderItems(List<OrderItemRequest> orderedItems);
}
