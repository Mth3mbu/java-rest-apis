package com.sales.web.Api.services.interfaces;

import com.sales.web.Api.responses.OrderResponse;

import java.util.List;

public interface OrderService {
    List<OrderResponse> getAllOrders();
    List<OrderResponse> getByUserId(long userId);
}
