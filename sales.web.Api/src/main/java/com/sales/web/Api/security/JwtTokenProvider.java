package com.sales.web.Api.security;

import com.sales.web.Api.exceptions.SalesApiException;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtTokenProvider {
    @Value("{app.jwt-secrete}")
    private String jwtSecrete;
    @Value("#{new Integer('${app.jwt-expiration}')}")
    private int expiration;

    public String generateToken(Authentication authentication){
        var username = authentication.getName();
        var currentDate = new Date();
        var expireDate = new Date(currentDate.getTime()+expiration);

        var token = Jwts.builder()
                .setSubject(username)
                .setIssuedAt(currentDate)
                .setExpiration(expireDate)
                .signWith(SignatureAlgorithm.HS512,jwtSecrete)
                .compact();

        return  token;
    }

    public String getUserNameFromJwtToken(String token){
        var claims = Jwts.parser()
                .setSigningKey(jwtSecrete)
                .parseClaimsJws(token)
                .getBody();

        return claims.getSubject();
    }

    public boolean validateToken(String token){
        try {
            Jwts.parser()
                    .setSigningKey(jwtSecrete)
                    .parseClaimsJws(token);
            return  true;
        }catch (SignatureException ex){
            throw new SalesApiException("Invalid JWT signature");
        }catch (MalformedJwtException ex){
            throw new SalesApiException("Invalid JWT token");
        }catch (ExpiredJwtException ex){
            throw new SalesApiException("Expired JWT token");
        }catch (UnsupportedJwtException ex){
            throw new SalesApiException("Unsupported JWT token");
        }catch (IllegalArgumentException ex){
            throw new SalesApiException("JWT claims string is empty");
        }
    }
}

