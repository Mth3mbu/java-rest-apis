package com.sales.web.Api.repositories;

import com.sales.web.Api.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product,Long> {
}
