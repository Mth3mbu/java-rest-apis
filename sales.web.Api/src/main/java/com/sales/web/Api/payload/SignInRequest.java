package com.sales.web.Api.payload;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class SignInRequest {
    @NotEmpty(message = "Please provide email")
    @Email(message = "Invalid email address")
    private String email;
    @NotEmpty(message = "Please provide a password")
    @Size(min = 6)
    private String password;
}
