package com.sales.web.Api.responses;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OrderItemResponse {
    private long id;
    private int quantity;
    private OrderProductResponse product;
}
