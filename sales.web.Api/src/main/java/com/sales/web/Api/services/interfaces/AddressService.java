package com.sales.web.Api.services.interfaces;

import com.sales.web.Api.payload.AddressDto;

public interface AddressService {
    AddressDto createAddress(AddressDto addressDto,long userId);
}
