package com.sales.web.Api.services.interfaces;

import com.sales.web.Api.payload.ProductDto;
import com.sales.web.Api.responses.ProductResponse;

public interface ProductService {
    ProductDto createProduct(ProductDto productDto);
    ProductDto updateProduct(ProductDto productDto, long productId);
    ProductResponse getAllProducts(int pageNo, int pageSize);
}
