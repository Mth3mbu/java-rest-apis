package com.sales.web.Api.responses;

import com.sales.web.Api.payload.ProductDto;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ProductResponse {
    private List<ProductDto> content;
    private int pageNo;
    private int pageSize;
    private long totalRecords;
    private int totalPages;
    private boolean isLast;
}
