package com.sales.web.Api.responses;
import com.sales.web.Api.payload.StatusDto;
import lombok.Data;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
public class OrderResponse {
    private long id;
    private long order_no;
    private Date created_date;
    private OrderUserResponse user;
    private StatusDto order_status;
    private Set<OrderItemResponse > items = new HashSet<>();
}
