package com.sales.web.Api.controllers;

import com.sales.web.Api.payload.SignInRequest;
import com.sales.web.Api.payload.SignUpRequest;

import com.sales.web.Api.responses.JwtAuthResponse;
import com.sales.web.Api.responses.UserResponse;
import com.sales.web.Api.security.JwtTokenProvider;
import com.sales.web.Api.services.interfaces.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(value = "Auth Controller exposes Sign in and Sign up rest end points")
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private UserService userService;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @ApiOperation(value = "Rest end point to register or sign up new users to sales application")
    @PostMapping("/signup")
    public ResponseEntity<UserResponse> signUp(@Valid @RequestBody SignUpRequest request){
        return new ResponseEntity<>(userService.createUser(request),HttpStatus.CREATED);
    }

    @ApiOperation(value = "Rest end point to login or sign in existing users to sales application, it returns a token that they can use to access the rest of the application")
    @PostMapping("/signin")
    public ResponseEntity<JwtAuthResponse> login(@Valid @RequestBody SignInRequest signInRequest){
        var auth= authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(signInRequest.getEmail(),signInRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(auth);

        var token = jwtTokenProvider.generateToken(auth);

        return ResponseEntity.ok(new JwtAuthResponse(token,"Bearer"));
    }
}
