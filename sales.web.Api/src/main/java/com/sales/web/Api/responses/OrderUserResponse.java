package com.sales.web.Api.responses;
import lombok.Data;

@Data
public class OrderUserResponse {
    private long id;
    private String email;
    private String name;
    private String last_name;
}
