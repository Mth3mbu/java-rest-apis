package com.sales.web.Api.payload;

import com.sales.web.Api.models.Order;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderItemDto {
    private int quantity;
    private ProductDto product;
    private Order order;
}
