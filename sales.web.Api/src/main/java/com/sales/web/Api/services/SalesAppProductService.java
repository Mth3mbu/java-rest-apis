package com.sales.web.Api.services;

import com.sales.web.Api.exceptions.ResourceNotFoundException;
import com.sales.web.Api.models.Product;
import com.sales.web.Api.payload.ProductDto;
import com.sales.web.Api.repositories.ProductRepository;
import com.sales.web.Api.responses.ProductResponse;
import com.sales.web.Api.services.interfaces.ProductService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class SalesAppProductService implements ProductService {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ModelMapper mapper;

    @Override
    public ProductDto createProduct(ProductDto productDto) {
        var product = productRepository.save(mapper.map(productDto, Product.class));

        return mapper.map(product,ProductDto.class);
    }

    @Override
    public ProductDto updateProduct(ProductDto productDto, long productId) {
        var existingProduct = productRepository.findById(productId)
                .orElseThrow(()->new ResourceNotFoundException("Product","id",String.format("%s",productId)));

        existingProduct.setDescription(productDto.getDescription());
        existingProduct.setName(productDto.getName());
        existingProduct.setPrice(productDto.getPrice());
        existingProduct.setImage(productDto.getImage());
        existingProduct.setQuantity(productDto.getQuantity());

        var updatedProduct = productRepository.save(existingProduct);

        return mapper.map(updatedProduct,ProductDto.class);
    }

    @Override
    public ProductResponse getAllProducts(int pageNo, int pageSize) {
        var pageable = PageRequest.of(pageNo,pageSize);
        var products = productRepository.findAll(pageable);

        var productsList= products.stream()
                .map(product -> mapper.map(product,ProductDto.class))
                .collect(Collectors.toList());

        return new ProductResponse(productsList,
                products.getNumber(),
                products.getSize(),
                products.getTotalElements(),
                products.getTotalPages(),
                products.isLast());
    }
}
