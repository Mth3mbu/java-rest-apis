package com.sales.web.Api.services.interfaces;

import com.sales.web.Api.payload.SignUpRequest;
import com.sales.web.Api.responses.UserResponse;

import java.util.List;

public interface UserService {
  UserResponse createUser(SignUpRequest userDto);
  List<UserResponse> getAllUsers();
}
