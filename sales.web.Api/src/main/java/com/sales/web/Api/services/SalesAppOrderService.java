package com.sales.web.Api.services;

import com.sales.web.Api.models.Order;
import com.sales.web.Api.payload.StatusDto;
import com.sales.web.Api.repositories.OrderRepository;
import com.sales.web.Api.responses.OrderItemResponse;
import com.sales.web.Api.responses.OrderProductResponse;
import com.sales.web.Api.responses.OrderResponse;
import com.sales.web.Api.responses.OrderUserResponse;
import com.sales.web.Api.services.interfaces.OrderService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SalesAppOrderService implements OrderService {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private ModelMapper mapper;

    @Override
    public List<OrderResponse> getAllOrders() {
        var orders = orderRepository.findAll();

        return  mapEntityToResponse(orders);
    }

    @Override
    public List<OrderResponse> getByUserId(long userId) {
        var orders = orderRepository.findByUserId(userId);

        return mapEntityToResponse(orders);
    }

    private List<OrderResponse> mapEntityToResponse(List<Order> orders){
        return orders.stream().map(order -> {
            var oderResponse = new OrderResponse();
            oderResponse.setCreated_date(order.getCreated_date());
            oderResponse.setUser(mapper.map(order.getUser(), OrderUserResponse.class));

            var orderItems = order.getItems()
                    .stream().map(orderItem ->
                            new OrderItemResponse(orderItem.getId(),
                                    orderItem.getQuantity(),
                                    mapper.map(orderItem.getProduct(), OrderProductResponse.class))
                    )
                    .collect(Collectors.toList());

            oderResponse.setItems(new HashSet<>(orderItems));
            oderResponse.setOrder_no(order.getOrder_no());
            oderResponse.setId(order.getId());
            oderResponse.setOrder_status(mapper.map(order.getStatus().getStatus(), StatusDto.class));

            return  oderResponse;
        }).collect(Collectors.toList());
    }
}
