package com.sales.web.Api.services;

import com.sales.web.Api.exceptions.ResourceNotFoundException;
import com.sales.web.Api.models.OrderStatus;
import com.sales.web.Api.payload.OrderStatusDto;
import com.sales.web.Api.payload.OrderStatusRequest;
import com.sales.web.Api.repositories.OrderRepository;
import com.sales.web.Api.repositories.OrderStatusRepository;
import com.sales.web.Api.repositories.StatusRepository;
import com.sales.web.Api.services.interfaces.OrderStatusService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SalesAppOrderStatusService implements OrderStatusService {
    @Autowired
    private OrderStatusRepository orderStatusRepository;
    @Autowired
    private StatusRepository statusRepository;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private ModelMapper mapper;

    @Override
    public OrderStatusDto createOrderStatus(OrderStatusRequest request) {

        var order = orderRepository.findById(request.getOrder_id())
                .orElseThrow(()->
                        new ResourceNotFoundException("Order","id",String.format("%s",request.getOrder_id())));

        var status = statusRepository.findById(request.getStatus_id())
                .orElseThrow(()->
                        new ResourceNotFoundException("Status","id",String.format("%s",request.getOrder_id())));

        var newOrderStatus = new OrderStatus();

        newOrderStatus.setStatus(status);
        newOrderStatus.setOrder(order);

        var orderStatus = orderStatusRepository.save(newOrderStatus);

        return mapper.map(orderStatus,OrderStatusDto.class);
    }
}
