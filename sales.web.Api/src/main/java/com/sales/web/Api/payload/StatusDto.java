package com.sales.web.Api.payload;

import lombok.Data;

@Data
public class StatusDto {
    private long id;
    private String name;
}
