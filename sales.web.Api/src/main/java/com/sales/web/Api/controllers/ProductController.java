package com.sales.web.Api.controllers;

import com.sales.web.Api.payload.ProductDto;
import com.sales.web.Api.responses.ProductResponse;
import com.sales.web.Api.services.interfaces.ProductService;
import com.sales.web.Api.utils.AppConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(value = "Product Controller exposes products CRUD operation ")
@RestController
@RequestMapping("/api/products")
public class ProductController {
    @Autowired
    private ProductService productService;

    @ApiOperation(value = "Adds new product to the Database")
    @PostMapping
    public ResponseEntity<ProductDto> createProduct(@Valid @RequestBody ProductDto productDto){
        return new ResponseEntity<>(productService.createProduct(productDto), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Updates existing product")
    @PutMapping("/{productId}")
    public ResponseEntity<ProductDto> updateProduct(@Valid @RequestBody ProductDto productDto, @PathVariable(name = "productId") long productId){
        return new ResponseEntity<>(productService.updateProduct(productDto,productId), HttpStatus.CREATED);
    }

    @ApiOperation(value = "get all products, it also has optional pagination query params")
    @GetMapping
    public ResponseEntity<ProductResponse> getAllProducts(@RequestParam(name = "pageNo", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER, required = false) int pageNo,
                                                          @RequestParam(name = "pageSize", defaultValue = AppConstants.DEFAULT_PAGE_SIZE, required = false) int pageSize){
        return new ResponseEntity<>(productService.getAllProducts(pageNo,pageSize), HttpStatus.OK);
    }
}
