package com.sales.web.Api.exceptions;

public class SalesApiException extends RuntimeException {
    private String message;

    public SalesApiException(String message) {
        this.message = message;
    }

    public String getMessage(){
        return  message;
    }
}
