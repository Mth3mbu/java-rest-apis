package com.sales.web.Api.payload;

import lombok.Data;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class UserDto {
    @NotEmpty(message = "Please provide an email")
    @Email(message = "Invalid email")
    private String email;

    @NotEmpty(message = "Please provide an email")
    @Size(min = 6, message = "Password must be at least 6 characters long")
    private String password;

    @NotEmpty(message = "Please provide a name")
    private String name;

    @NotEmpty(message = "Please provide a last name")
    private String last_name;
}
