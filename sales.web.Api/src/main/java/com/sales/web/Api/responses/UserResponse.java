package com.sales.web.Api.responses;

import com.sales.web.Api.payload.AddressDto;
import com.sales.web.Api.payload.UserDto;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserResponse {
    private UserDto user;
    private AddressDto address;
}
