package com.sales.web.Api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Arrays;
import java.util.List;

import static com.sales.web.Api.utils.AppConstants.AUTHORIZATION_HEADER;

@Configuration
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .securityContexts(Arrays.asList(securityContext()))
                .securitySchemes(Arrays.asList(apiKey()))
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }

    private ApiKey apiKey(){
        return new ApiKey("JWT",AUTHORIZATION_HEADER, "header");
    }

    private ApiInfo apiInfo(){
        return  new ApiInfo("Spring boot Sales Rest API",
                "Online Store Sales Api Documentation",
                "1",
                "@Copy rights reserved sales Api 2022",
                "Jackaranda Mthembu",
                "MIT",
                "");
    }

    private SecurityContext securityContext(){
        return  SecurityContext.builder().securityReferences(defaultSecurityRefs()).build();
    }

    private List<SecurityReference> defaultSecurityRefs(){
        var authorizationScope = new AuthorizationScope("global","accessEverything");
        var authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0]=authorizationScope;

        return Arrays.asList(new SecurityReference("JWT",authorizationScopes));
    }
}
