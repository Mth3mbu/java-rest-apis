package com.sales.web.Api.responses;

import lombok.Data;

@Data
public class OrderProductResponse {
    private long id;
    private String name;
    private String image;
    private double price;
}
