package com.sales.web.Api.services.interfaces;

import com.sales.web.Api.payload.OrderStatusDto;
import com.sales.web.Api.payload.OrderStatusRequest;
import org.springframework.stereotype.Service;

@Service
public interface OrderStatusService {
    OrderStatusDto createOrderStatus(OrderStatusRequest request);
}
