package com.sales.web.Api.repositories;

import com.sales.web.Api.models.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address,Long> {
}
