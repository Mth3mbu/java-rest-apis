package com.sales.web.Api.services;

import com.sales.web.Api.exceptions.SalesApiException;
import com.sales.web.Api.models.User;
import com.sales.web.Api.payload.AddressDto;
import com.sales.web.Api.payload.SignUpRequest;
import com.sales.web.Api.payload.UserDto;
import com.sales.web.Api.repositories.UserRepository;
import com.sales.web.Api.responses.UserResponse;
import com.sales.web.Api.services.interfaces.AddressService;
import com.sales.web.Api.services.interfaces.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SalesAppUserService implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AddressService addressService;
    @Autowired
    private ModelMapper mapper;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserResponse createUser(SignUpRequest request) {
        if(userRepository.existsByEmail(request.getUser().getEmail())){
            throw new SalesApiException(String.format("Email %S already exists",request.getUser().getEmail()));
        }

        var newUser = mapper.map(request.getUser(), User.class);
        newUser.setPassword(passwordEncoder.encode(newUser.getPassword()));

        var user= userRepository.save(newUser);
        var address =   addressService.createAddress(request.getAddress(),user.getId());

        var response = new UserResponse(mapper.map(user, UserDto.class), address);

        return response;
    }

    @Override
    public List<UserResponse> getAllUsers() {
        var allUsers = userRepository.findAll();

       return allUsers.stream()
               .map(user-> {
                   user.setPassword("**************");
                   return new UserResponse(mapper.map(user,UserDto.class),mapper.map(user.getAddress(), AddressDto.class));
               })
               .collect(Collectors.toList());
    }
}
