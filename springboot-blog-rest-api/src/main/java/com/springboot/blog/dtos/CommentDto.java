package com.springboot.blog.dtos;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
public class CommentDto {
    private long id;
    @NotEmpty(message = "Please provide a Name")
    private String name;
    @NotEmpty(message = "Please provide an Email")
    @Email(message = "Please provide a valid Email")
    private String email;
    @NotEmpty(message = "Please provide a comment body")
    private String body;
}
