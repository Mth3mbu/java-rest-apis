package com.springboot.blog.dtos;

import java.util.Date;

public class ErrorDetails {
    private Date timestamp;
    private String message;
    private String url;

    public ErrorDetails(Date timestamp, String message, String details) {
        this.timestamp = timestamp;
        this.message = message;
        this.url = details;
    }

    public String getUrl() {
        return url;
    }

    public String getMessage() {
        return message;
    }

    public Date getTimestamp() {
        return timestamp;
    }
}
