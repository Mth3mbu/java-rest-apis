package com.springboot.blog.dtos;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class PostDto {
    private Long id;
    @NotNull
    @NotEmpty(message = "Please provide Title")
    @Size(min = 2, message = "Title should have at least have 2 characters")
    private String title;

    @NotNull
    @NotEmpty(message = "Please provide Description")
    @Size(min = 2, message = "Description should have at least have 10 characters")
    private String description;

    @NotEmpty(message = "Please provide content")
    private String content;
}
