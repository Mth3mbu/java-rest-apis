package com.springboot.blog.services;

import com.springboot.blog.dtos.RegisterDto;
import com.springboot.blog.exceptions.BlogExceptionApiException;
import com.springboot.blog.models.User;
import com.springboot.blog.repositories.RoleRepository;
import com.springboot.blog.repositories.UserRepository;
import com.springboot.blog.services.Interfaces.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class UserServiceImpl implements UserService {
    private ModelMapper mapper;
    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private PasswordEncoder passwordEncoder;

    public UserServiceImpl(ModelMapper mapper, UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.mapper = mapper;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public RegisterDto register(RegisterDto registerDto) {

        if(userRepository.existsByUsername(registerDto.getUsername())){
            throw new BlogExceptionApiException(String.format("%s already exists", registerDto.getUsername()));
        }

        if(userRepository.existsByEmail(registerDto.getEmail())){
            throw new BlogExceptionApiException(String.format("%s already exists", registerDto.getEmail()));
        }

        var role = roleRepository.findByName("ROLE_ADMIN").get();

        registerDto.setPassword(passwordEncoder.encode(registerDto.getPassword()));
        var user = mapper.map(registerDto,User.class);
        user.setRoles(Collections.singleton(role));

        var response= userRepository.save(user);

        return mapper.map(response,RegisterDto.class);
    }
}
