package com.springboot.blog.services.Interfaces;

import com.springboot.blog.dtos.CommentDto;

import java.util.List;

public interface CommentService {
    CommentDto createComment(long postId, CommentDto comment);
    List<CommentDto> findByPostId(long postId);
    CommentDto findByCommentId(long postId, long commentId);
}
