package com.springboot.blog.services.Interfaces;

import com.springboot.blog.dtos.PostDto;
import com.springboot.blog.models.PostResponse;

import java.util.List;

public interface PostService {
    PostDto createPost(PostDto post);
    PostDto updatePost(PostDto post, long id);
    String deletePost(long id);
    List<PostDto> getAllPosts();
    PostResponse getAllPosts(int pageNo, int pageSize,String sortBy,String sortDir);
    PostDto getPostById(Long id);
}
