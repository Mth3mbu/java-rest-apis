package com.springboot.blog.services.Interfaces;

import com.springboot.blog.dtos.RegisterDto;
import com.springboot.blog.models.User;

public interface UserService {
    RegisterDto register(RegisterDto registerDto);
}
