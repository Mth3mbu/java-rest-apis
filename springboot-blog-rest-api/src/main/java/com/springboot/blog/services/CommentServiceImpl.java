package com.springboot.blog.services;

import com.springboot.blog.dtos.CommentDto;
import com.springboot.blog.exceptions.ResourceNotFoundException;
import com.springboot.blog.models.Comment;
import com.springboot.blog.repositories.CommentRepository;
import com.springboot.blog.repositories.PostRepository;
import com.springboot.blog.services.Interfaces.CommentService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommentServiceImpl implements CommentService {
    private CommentRepository commentRepository;
    private PostRepository postRepository;
    private ModelMapper mapper;

    public CommentServiceImpl(CommentRepository commentRepository, PostRepository postRepository, ModelMapper mapper) {
        this.commentRepository = commentRepository;
        this.postRepository = postRepository;
        this.mapper = mapper;
    }

    @Override
    public CommentDto createComment(long postId, CommentDto request) {
        var comment = mapToEntity(request);
        var post = postRepository.findById(postId)
                .orElseThrow(()->new ResourceNotFoundException("Post","id", String.format("%S", postId)));

        comment.setPost(post);

        var response = commentRepository.save(comment);

        return mapToDto(response);
    }

    @Override
    public List<CommentDto> findByPostId(long postId) {
        var comments = commentRepository.findByPostId(postId);

        return comments.stream()
                .map(comment -> mapToDto(comment))
                .collect(Collectors.toList());
    }

    @Override
    public CommentDto findByCommentId(long postId, long commentId) {
        postRepository.findById(postId)
                .orElseThrow(()->new ResourceNotFoundException("Comment","id", String.format("%S", postId)));

        var comment = commentRepository.findById(commentId)
                .orElseThrow(()->new ResourceNotFoundException("Comment","id", String.format("%S", commentId)));

        return mapToDto(comment);
    }

    private Comment mapToEntity(CommentDto commentDto){
        return mapper.map(commentDto, Comment.class);
    }

    private CommentDto mapToDto(Comment comment){
        return mapper.map(comment, CommentDto.class);
    }
}
