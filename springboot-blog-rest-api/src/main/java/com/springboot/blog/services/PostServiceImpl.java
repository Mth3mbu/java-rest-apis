package com.springboot.blog.services;

import com.springboot.blog.dtos.PostDto;
import com.springboot.blog.exceptions.ResourceNotFoundException;
import com.springboot.blog.models.Post;
import com.springboot.blog.models.PostResponse;
import com.springboot.blog.repositories.PostRepository;
import com.springboot.blog.services.Interfaces.PostService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PostServiceImpl implements PostService {
    private PostRepository postRepository;
    private ModelMapper mapper;

    @Autowired
    public PostServiceImpl(PostRepository postRepository, ModelMapper mapper) {
        this.postRepository = postRepository;
        this.mapper = mapper;
    }

    @Override
    public PostDto createPost(PostDto postDto) {
         var request = mapToEntity(postDto);
         var postResponse=  postRepository.save(request);
         var response = mapToDto(postResponse);

        return response;
    }

    @Override
    public PostDto updatePost(PostDto postDto, long id) {
        var request = mapToEntity(postDto);
        var post = postRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Post","id", String.format("%S", id)));
        post.setTitle(postDto.getTitle());
        post.setContent(postDto.getDescription());
        post.setDescription(postDto.getDescription());

        var updatedPost = postRepository.save(post);
        return mapToDto(updatedPost);
    }

    @Override
    public String deletePost(long id) {
        var post = postRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Post","id", String.format("%S", id)));
        postRepository.delete(post);

        return String.format("Post with id: %S deleted successfully",id);
    }

    @Override
    public List<PostDto> getAllPosts() {
        var response = postRepository.findAll();
        var posts = response.stream().map(post-> mapToDto(post)).collect(Collectors.toList());

        return  posts;
    }

    @Override
    public PostResponse getAllPosts(int pageNo, int pageSize,String sortBy,String sortDir) {
        var sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name())? Sort.by(sortBy).ascending(): Sort.by(sortBy).descending();
        var pageable = PageRequest.of(pageNo,pageSize, sort);
        Page<Post> postPages = postRepository.findAll(pageable);
        List<Post> posts = postPages.getContent();

        List<PostDto> content = posts.stream().map(post -> mapToDto(post)).collect(Collectors.toList());
        PostResponse postResponse = new PostResponse();
        postResponse.setContent(content);
        postResponse.setPageNo(postPages.getNumber());
        postResponse.setPageSize(postPages.getSize());
        postResponse.setTotalPages(postPages.getTotalPages());
        postResponse.setLast(postPages.isLast());
        postResponse.setTotalRecords(postPages.getTotalElements());

        return postResponse;
    }

    @Override
    public PostDto getPostById(Long id) {
        var post = postRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Post","id", String.format("%S", id)));

        return mapToDto(post);
    }

    private PostDto mapToDto(Post post){
        return mapper.map(post, PostDto.class);
    }

    private  Post mapToEntity(PostDto postDto){
        return  mapper.map(postDto, Post.class);
    }
}
