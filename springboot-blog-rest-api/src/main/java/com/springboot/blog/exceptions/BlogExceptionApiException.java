package com.springboot.blog.exceptions;

public class BlogExceptionApiException extends RuntimeException {
    private String message;

    public BlogExceptionApiException(String message) {
        this.message = message;
    }

    public String getMessage(){
        return  message;
    }
}
