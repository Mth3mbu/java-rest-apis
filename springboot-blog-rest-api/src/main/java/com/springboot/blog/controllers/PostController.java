package com.springboot.blog.controllers;

import com.springboot.blog.dtos.PostDto;
import com.springboot.blog.models.PostResponse;
import com.springboot.blog.services.Interfaces.PostService;
import com.springboot.blog.utils.AppConstants;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/posts")
public class PostController {

    private PostService postService;

    public PostController(PostService postService) {
        this.postService = postService;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping
    public ResponseEntity<PostDto> createPost(@Valid @RequestBody PostDto post){

        return  new ResponseEntity<>(postService.createPost(post), HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<PostDto> updatePost(@Valid @RequestBody PostDto post, @PathVariable(name = "id") long id){

        return  new ResponseEntity<>(postService.updatePost(post,id), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<PostDto>> GetPosts(){

        return  new ResponseEntity<>(postService.getAllPosts(), HttpStatus.OK);
    }

    @GetMapping("/pagination")
    public ResponseEntity<PostResponse> GetPosts(@RequestParam(name = "pageNo", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER, required = false) int pageNo,
                                                 @RequestParam(name = "pageSize", defaultValue = AppConstants.DEFAULT_PAGE_SIZE, required = false) int pageSize,
                                                 @RequestParam(name = "sortBy", defaultValue = AppConstants.DEFAULT_SORT_PARAM, required = false) String sortBy,
                                                 @RequestParam(name = "sortDir", defaultValue = AppConstants.DEFAULT_SORT_DIRECTION, required = false) String sortDir
                                                  ){

        return  new ResponseEntity<>(postService.getAllPosts(pageNo,pageSize,sortBy,sortDir), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PostDto> GetPostById(@PathVariable(name = "id") long id){

        return  new ResponseEntity<>(postService.getPostById(id), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deletePostById(@PathVariable(name = "id") long id){

        return  new ResponseEntity<>(postService.deletePost(id), HttpStatus.OK);
    }
}
