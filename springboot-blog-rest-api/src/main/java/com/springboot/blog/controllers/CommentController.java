package com.springboot.blog.controllers;

import com.springboot.blog.dtos.CommentDto;
import com.springboot.blog.services.Interfaces.CommentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CommentController {

    private CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping("/posts/{postId}/comments")
    public ResponseEntity<CommentDto> create(
            @PathVariable(name = "postId") long postId,
            @Valid @RequestBody CommentDto commentDto){

        return new ResponseEntity<>(commentService.createComment(postId,commentDto), HttpStatus.CREATED);
    }

    @GetMapping("/posts/{postId}/comments")
    public ResponseEntity<List<CommentDto>> getByPostId(
            @PathVariable(name = "postId") long postId){

        return new ResponseEntity<>(commentService.findByPostId(postId), HttpStatus.OK);
    }

    @GetMapping("/posts/{postId}/comments/{commentId}")
    public ResponseEntity<CommentDto> getByPostIdAndCommentId(
            @PathVariable(name = "postId") long postId,
            @PathVariable(name = "commentId") long commentId){

        return new ResponseEntity<>(commentService.findByCommentId(postId,commentId), HttpStatus.OK);
    }
}
